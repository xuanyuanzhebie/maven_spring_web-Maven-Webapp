/**
 * js集成机制
 * @param jsonClass
 * @param prop
 * @returns {F}
 */

function testextens(jsonClass, prop) {
//	debugger;
	function F() {

	}
	;
	if (typeof (jsonClass == 'object')) {
		for ( var i in jsonClass) {
			F.prototype[i] = jsonClass[i];
		}

	}
	if (typeof (jsonClass) == 'function') {
		F.prototype = jsonClass.prototype;
		for ( var i in prop) {
			F.prototype[i] = prop[i];
		}
	}
	return F;
}

var person = testextens({
	setName : function(name) {

		this.name = name;
	},
	getName : function() {
		return this.name;
	}
});

var student = testextens(person, {
	setId : function(id) {
		this.id = id;
	},
	getId : function() {

		return this.id;
	}
});
var p = new person();
p.setName('smn');
alert(p.getName());
var s = new student();
s.setId('1');
s.setName('zkj');
alert(s.getId());
alert(s.getName());