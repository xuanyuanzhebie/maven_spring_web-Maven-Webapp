package com.zkj.spring.scope;

import java.util.ArrayList;
import java.util.List;

public class HelloWorld_ScopeList {

	public List<String> sList = new ArrayList<String>();

	public HelloWorld_ScopeList() {
		System.out.println("create hello scope");
	}

	public List<String> getsList() {
		return sList;
	}

	public void setsList(List<String> sList) {
		this.sList = sList;
	}

}
