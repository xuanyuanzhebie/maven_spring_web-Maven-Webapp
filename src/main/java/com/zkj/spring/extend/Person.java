package com.zkj.spring.extend;

import java.util.List;
import java.util.Map;

public class Person {
	public Person() {

		System.out.println("person_extends is be created");
	}

	private String name;

	public String age;

	private List listes;

	private Map map;

	public List getListes() {
		return listes;
	}

	public void setListes(List listes) {
		this.listes = listes;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public String getAge() {

		return age;

	}

	public void setAge(String age) {

		this.age = age;

	}

}
