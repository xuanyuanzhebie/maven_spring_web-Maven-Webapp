package com.zkj.spring.zhuanzhang;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class AccountDAOImp extends JdbcDaoSupport implements AccountDAO {

	@Override
	public void out(String outAccount, double money) {
		String sql = " update account t set  " + " t.money=t.money-?  where t.name=? ";
		this.getJdbcTemplate().update(sql,  money,outAccount);
	}

	@Override
	public void in(String inAccount, double money) {
		String sql = " update account t " + " set t.money=t.money+?  where t.name=? ";
		this.getJdbcTemplate().update(sql,money,inAccount);

	}

}
