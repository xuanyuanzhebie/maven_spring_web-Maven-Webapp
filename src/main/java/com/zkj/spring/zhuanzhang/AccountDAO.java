package com.zkj.spring.zhuanzhang;

public interface AccountDAO {

	/**
	 * 转出
	 * 
	 * @param outAccount
	 * @param money
	 */
	public void out(String outAccount, double money);

	/**
	 * 转入
	 * 
	 * @param in
	 * @param money
	 */
	public void in(String in, double money);
}
