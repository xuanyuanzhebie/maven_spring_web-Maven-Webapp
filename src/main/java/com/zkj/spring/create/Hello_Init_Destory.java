package com.zkj.spring.create;

public class Hello_Init_Destory {

	public Hello_Init_Destory() {
		System.out.println("init lost control");
	}

	private void init() {

		System.out.println("init");
	}

	public void destroy() {

		System.out.println("destroy");
	}
}
