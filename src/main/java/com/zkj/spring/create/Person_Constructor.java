package com.zkj.spring.create;

public class Person_Constructor {

	private Long index;
	private String name;

	private Person_Constructor(Long index, String name) {
		this.index = index;
		this.name = name;

	}

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
