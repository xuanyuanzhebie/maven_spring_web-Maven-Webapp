package com.zkj.spring.create;

import com.zkj.spring.bean.Person;

/**
 * 静态工厂方法创建对象
 * 
 * @author zkj
 * 
 */
public class HelloWorldFactory {

	public static Person getInstance() {

		return new Person();
	}
}
