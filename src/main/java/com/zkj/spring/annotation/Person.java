package com.zkj.spring.annotation;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Person {

	private Long pid;

	// @Resource(name = "student")
	@Autowired
	@Qualifier(value = "student")
	private Student student;

	public void show() {

		this.student.studentShow();
	}
}
