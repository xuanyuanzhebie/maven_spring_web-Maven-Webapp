package com.zkj.spring.aspect;

import org.aspectj.lang.ProceedingJoinPoint;

public class AspectDemo {

	public void afterRunMethod(Object val) {
		System.out.println("目标方法返回值" + val);

		System.out.println("后置通知");
	}

	public void aroundMethod(ProceedingJoinPoint joinPoint) {

		try {
			System.out.println("around beofore method");
			joinPoint.proceed();
			System.out.println("around after method");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
