package com.zkj.spring.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zkj.spring.extend.Person;
import com.zkj.spring.extend.Student_Extend;

/**
 * 测试spring类之间继承关系
 * 
 * 测试spring不能为抽象类创建对象
 * 
 * @author zkj
 * 
 */
public class SpringExtendTest {

	@Test
	public void testSpringExtends() {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		Person p = (Person) context.getBean("person_extends");
		Student_Extend s = (Student_Extend) context.getBean("student_extend");
		System.out.println(s.getName());
	}
}
