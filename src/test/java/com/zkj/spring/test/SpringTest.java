package com.zkj.spring.test;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zkj.spring.aspect.TargetClass;
import com.zkj.spring.bean.Person;
import com.zkj.spring.bean.Student;
import com.zkj.spring.create.Hello_Init_Destory;
import com.zkj.spring.scope.HelloWorld_Scope;
import com.zkj.spring.scope.HelloWorld_ScopeList;
import com.zkj.spring.transaction.ClassesServices;
import com.zkj.spring.when.HelloWhenCreate;

public class SpringTest {

	@Test
	public void testZhuanZhang() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		TargetClass cs = (TargetClass) ac.getBean("targetClass");
		cs.targertMethod();
	}

	@Test
	public void testAroundMethod() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		TargetClass cs = (TargetClass) ac.getBean("targetClass");
		cs.targertMethod();
	}

	@Test
	public void testMyException() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		ClassesServices cs = (ClassesServices) ac.getBean("classesServices");
		cs.saveClasses();
	}

	@Test
	public void testTransaction() {

		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		ClassesServices classServices = (ClassesServices) ac.getBean("classesServices");
		classServices.saveClasses();
	}

	@Test
	public void testAspect() {
		// 后置通知
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		TargetClass tc = (TargetClass) ac.getBean("targetClass");
		tc.targertMethod();
	}

	@Test
	public void testAnnotation() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		com.zkj.spring.annotation.Person p = (com.zkj.spring.annotation.Person) ac.getBean("person");
		p.show();
	}

	@Test
	public void PersonTest() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		Person p = (Person) bf.getBean("person");
		System.out.println(p.getName() + ":" + p.getAge());
	}

	@Test
	public void factoryTest() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		// Person hf = (Person) bf.getBean("HelloWorld_Factory");
	}

	@Test
	public void HelloWorld_Scope() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		HelloWorld_Scope hs = (HelloWorld_Scope) bf.getBean("HelloWorld_Scope");
		HelloWorld_Scope hs2 = (HelloWorld_Scope) bf.getBean("HelloWorld_Scope");
		System.out.println(hs);
		System.out.println(hs2);

	}

	@Test
	public void HelloWorld_ScopeList() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		com.zkj.spring.scope.HelloWorld_ScopeList hs = (HelloWorld_ScopeList) bf.getBean("HelloWorld_ScopeList");
		com.zkj.spring.scope.HelloWorld_ScopeList hs2 = (HelloWorld_ScopeList) bf.getBean("HelloWorld_ScopeList");
		hs.getsList().add("zkj");
		hs2.getsList().add("smn");
		System.out.println(hs.getsList().size());

	}

	@Test
	public void HelloWorld_When() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		HelloWhenCreate hs = (HelloWhenCreate) bf.getBean("HelloWorld_When");
		System.out.println(hs);
	}

	@Test
	public void HelloWorld_Init_Destroy() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		Hello_Init_Destory hs = (Hello_Init_Destory) bf.getBean("HelloWorld_Init_Destroy");
		ClassPathXmlApplicationContext c = (ClassPathXmlApplicationContext) bf;
		c.close();
	}

	@Test
	public void HelloWorld_InitLostControl() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		Hello_Init_Destory hs = (Hello_Init_Destory) bf.getBean("HelloWorld_InitLostControl");
		ClassPathXmlApplicationContext c = (ClassPathXmlApplicationContext) bf;
		c.close();
	}

	@Test
	public void Person_property_Test() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		Person p = (Person) bf.getBean("HelloWorld_Property");
		p.getStudent().studentShow();

	}

	@Test
	public void Person_list_property_Test() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		Person p = (Person) bf.getBean("HelloWorld_Property");

		Student s = (Student) p.getListes().get(2);
		// s.studentShow();

		Map m = (Map) p.getMap();
		Iterator e = m.entrySet().iterator();
		while (e.hasNext()) {

			Entry entry = (Entry) e.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}

	}

	@Test
	public void Person_Constructor() {
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		com.zkj.spring.create.Person_Constructor p = (com.zkj.spring.create.Person_Constructor) bf
				.getBean("Person_Constructor");
		System.out.println(p.getName() + p.getIndex());

	}
}
