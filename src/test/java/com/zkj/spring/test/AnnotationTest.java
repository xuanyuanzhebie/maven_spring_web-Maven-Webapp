package com.zkj.spring.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zkj.spring.zhuanzhang.AccountService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-zhuanzhang.xml")
public class AnnotationTest {

	@Autowired
	private AccountService accountService;

	@Test
	public void testZhuanZhang() {

		accountService.transfer("aaa", "bbb", 1000);

	}
}
